variable "storage_capacity" {
  description = "Amount of Allocated Storage for the FSx Share"
}

variable "subnet_ids" {
  description = "List of Subnet ID's for the FSx Share"
}

variable "preferred_subnet_id" {
  description = "Primary Subnet & AZ"
}

variable "throughput_capacity" {
  description = "Amount of dedicationed throughput capacity"
  default     = 16
}

variable "deployment_type" {
  description = "Mutli AZ or Single AZ"
  default     = "SINGLE_AZ_2"
}

variable "default_tags" {}

variable "username" {
  description = "Acitve Directory Service Account Username"
}

variable "password" {
  description = "Acitve Directory Service Account Password"
}

variable "fsx_ad_administrator_group" {
  description = "AD Security Group with Permissions to Administer the FSx share"
}

variable "ou_distinguished_name" {
  description = "Distinguised OU whhere the AD Service account has delegated permissions over"
}

variable "ingress_cidr_block" {
  description = "Ingress CIDR Block for FSx SG"
}

variable "vpc_id" {
  description = "VPC ID"
}