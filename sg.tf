resource "aws_security_group" "fsx_sg" {
  name        = "FSx SG"
  description = "FSx SG"
  vpc_id      = var.vpc_id

  # Allow outbound access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
  }

  # Allow FSx Required Ports inbound access.
  ingress {
    from_port   = 88
    to_port     = 88
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 135
    to_port     = 135
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 464
    to_port     = 464
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 636
    to_port     = 636
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 3268
    to_port     = 3269
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 9389
    to_port     = 9389
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 88
    to_port     = 88
    protocol    = "UDP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 123
    to_port     = 123
    protocol    = "UDP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "UDP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 464
    to_port     = 464
    protocol    = "UDP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "TCP"
    cidr_blocks = var.ingress_cidr_block
  }

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "UDP"
    cidr_blocks = var.ingress_cidr_block
  }

  tags = merge(var.default_tags)

}