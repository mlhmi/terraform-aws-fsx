# TERRAFORM-AWS-FSx

Module for the creation of a FSx Windows File Share 

## Usage v0.12.29 - FSx

### Main.tf

```terraform

module "test-fsx" {
  source                     = "./modules/fsx"
  storage_capacity           = 60
  subnet_ids                 = ["subnet-a1a11111"]
  preferred_subnet_id        = "subnet-a1a11111"
  throughput_capacity        = 16
  deployment_type            = "SINGLE_AZ_2"
  default_tags               = var.default_tags
  username                   = local.FSx_Credentials.username
  password                   = local.FSx_Credentials.password
  fsx_ad_administrator_group = "Engineering"
  ou_distinguished_name      = "OU=Computers,DC=example,DC=com"
  ingress_cidr_block         = ["0.0.0.0/0"]
  vpc_id                     = "vpc-1a1a1a11"
}
```


## Recommended Secret Manager Code Example

Please see the below example for storing your database credentials in AWS Secrets Manager. Store a new secret (Type:Other) in Plaintext and use the JSON example below. 

The TF code which proceeds is what you should use in your terraform code to gather the information required for Username and Password. This ensures our secrets are stored securely and not hard coded or pushed to any of our repo's. 

### JSON Secrets Manager Example

```JSON
{
    "username": "admin",
    "password": "password"
}

```

### Recommended Secrets Manager TF Code

```terraform

data "aws_secretsmanager_secret_version" "credentials" {
    secret_id               = "$NAME_OF_SECRET_IN_SECRETS_MANAGER"
}

locals {
    FSx_Credentials = jsondecode (
        data.aws_secretsmanager_secret_version.credentials.secret_string
    ) 
}

```


## Variables

- `storage_capacity`           - Amount of Allocated Storage for the FSx Share
- `subnet_ids`                 - List of Subnet ID's for the FSx Share
- `preferred_subnet_id`        - Primary Subnet & AZ
- `throughput_capacity`        - Amount of dedicationed throughput capacity - Default is 16Mbs
- `deployment_type`            - Mutli AZ or Single AZ
- `default_tags`               - Default Tags to be applied
- `username`                   - Acitve Directory Service Account Username
- `password`                   - Acitve Directory Service Account Password 
- `fsx_ad_administrator_group` - AD Security Group with Permissions to Administer the FSx share
- `ou_distinguished_name`      - TDistinguised OU whhere the AD Service account has delegated permissions over
- `ingress_cidr_block`         - Ingress CIDR Block for FSx SG"
- `vpc_id`                     - VPC ID

## Outputs

- `fsx_arn`                      - FSx ARN
- `fsx_dns_name`                 - FSx DNS Name
- `fsx_id`                       - FSx ID
- `fsx_eni_id`                   - FSx Network Interface ID
- `fsx_owner_id`                 - FSx OwnerID
- `fsx_vpc_id`                   - FSx VPC ID
- `fsx_preferred_file_server_ip` - FSx Preferred File Server IP

