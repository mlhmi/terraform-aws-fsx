##### AO World PLC Open - FSx Windows Network File System #####

### Data Lookup - FSx KMS Encryption Key ###

data "aws_kms_key" "fsx_kms" {
  key_id = "alias/aws/fsx"
}

### FSx Windows File Share ###

resource "aws_fsx_windows_file_system" "fsx_windows" {
  kms_key_id                        = data.aws_kms_key.fsx_kms.arn
  storage_capacity                  = var.storage_capacity
  subnet_ids                        = var.subnet_ids
  preferred_subnet_id               = var.preferred_subnet_id
  throughput_capacity               = var.throughput_capacity
  security_group_ids                = [aws_security_group.fsx_sg.id]
  deployment_type                   = var.deployment_type
  storage_type                      = "SSD"
  copy_tags_to_backups              = true
  daily_automatic_backup_start_time = "00:00"
  tags                              = var.default_tags


  self_managed_active_directory {
    dns_ips                                = ["10.10.200.211", "10.10.200.213"]
    domain_name                            = "drl.local"
    username                               = var.username
    password                               = var.password
    file_system_administrators_group       = var.fsx_ad_administrator_group
    organizational_unit_distinguished_name = var.ou_distinguished_name

  }


}