# Pull Request Process

1. Update the CHANGELOG.md with details of changes to the modules, or any new module additions.
2. Increase the version number in CHANGELOG.md to the new version that this Pull Request would
   represent. The versioning scheme we use is [SemVer](http://semver.org/).
3. Ensure that the module has a README.md present that details usage of the module, including
   descriptions for the arguments and example usage.
4. Push your changes on a new branch and submit a merge request. Once the merge request has been
   reviewed we will either feedback with suggestions/comments or merge the change.
