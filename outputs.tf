### FSx File Share Outputs ###

output "fsx_arn" {
  description = "FSx ARN"
  value       = aws_fsx_windows_file_system.fsx_windows.arn
}

output "fsx_dns_name" {
  description = "FSx DNS Name"
  value       = aws_fsx_windows_file_system.fsx_windows.dns_name
}

output "fsx_id" {
  description = "FSx ID"
  value       = aws_fsx_windows_file_system.fsx_windows.id
}

output "fsx_eni_id" {
  description = "FSx Network Interface ID"
  value       = aws_fsx_windows_file_system.fsx_windows.network_interface_ids
}

output "fsx_owner_id" {
  description = "FSx OwnerID"
  value       = aws_fsx_windows_file_system.fsx_windows.owner_id
}

output "fsx_vpc_id" {
  description = "FSx VPC ID"
  value       = aws_fsx_windows_file_system.fsx_windows.vpc_id
}





